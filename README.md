# Debian Hamradio Blend Live CD

The live CD we have on the website, at time of writing is oldstable.  I'm
putting together a collection of relevant scripts so we can work together on
making the best livedisk we can, and keeping it relevant isn't the
responsibility of any individual.

This is building a Buster image targeting 32bit x86 machines, with a pae kernel
to ensure there is maximum compatability.

# Build
## Prerequisites

This requires the excellent `live-build` toolset - it will do all the hard work
for you.

## Custom Package List

The list of packages we install are in
`./config/package-lists/hamradio.list.chroot`. If you need to add/remove any
packages, this is the file to edit.

Currently installed packages are: `xorg mate-desktop-environment firefox-esr
hamradio-antenna hamradio-datamodes hamradio-digitalvoice hamradio-logging
hamradio-morse hamradio-nonamateur hamradio-packetmodes hamradio-rigcontrol
hamradio-satellite hamradio-sdr hamradio-tools hamradio-training`

## Build Howto

Run `lb clean && lb build` in the project root directory as root user.

This will remove any superfluous files from the previous build and then build a
new image from scratch.

After some waiting, an iso shall be produced.

# Other Releases

Stable, oldstable and unstable build capability will be added through different
branches of the repo.
